#include <QCoreApplication>
#include <QCommandLineParser>
#include <QProcess>
#include <QSettings>
#include <QFile>
#include <QDir>
#include <QTextStream>

int main(int argc, char *argv[]) {
    int ret = 0;
    QCoreApplication app(argc, argv);

    QCommandLineOption optDbBinDir(QStringList() << "dbbindir", "local database executable directory", "directory");
    QCommandLineOption optDbDataDir(QStringList() << "dbdatadir", "local database data directory", "directory");
    QCommandLineOption optDbSvcName(QStringList() << "dbservice", "local database service name", "service", "ESMySQL");
    QCommandLineOption optDbRootPasswd(QStringList() << "rootpasswd", "local database root password", "password");
    QCommandLineOption optDbPort(QStringList() << "port", "local database TCP port", "port", "3306");
    QCommandLineOption optDatabaseName(QStringList() << "dbname", "database name", "name", "esdb");
    QCommandLineOption optConfigFileName(QStringList() << "outcfgfile", "output configuration file name", "file", "localdbcfg.xml");

    QCommandLineParser parser;
    parser.addOption(optDbBinDir);
    parser.addOption(optDbDataDir);
    parser.addOption(optDbSvcName);
    parser.addOption(optDbRootPasswd);
    parser.addOption(optDbPort);
    parser.addOption(optDatabaseName);
    parser.addOption(optConfigFileName);

    parser.process(app);

    if (parser.isSet(optDbBinDir) && parser.isSet(optDbDataDir)) {
        QString bindir = parser.value(optDbBinDir);
        QString dbdir = parser.value(optDbDataDir);
        QString svcName = parser.value(optDbSvcName);
        QString rootPasswd = parser.value(optDbRootPasswd);
        QString port = parser.value(optDbPort);
        QString dbname = parser.value(optDatabaseName);
        QString cfgFile = parser.value(optConfigFileName);

        QDir fileDir(dbdir);
        if (!fileDir.exists()) {
            if (!fileDir.mkpath(".")) {
                ret = -1;
            }
        }

        if (ret == 0) {
            QProcess proc;
            QStringList args;
            args.append("--datadir=" + dbdir);
            args.append("--service=" + svcName);
            args.append("--password=" + rootPasswd);
            args.append("--port=" + port);
            args.append("--allow-remote-root-access");
            ret = proc.execute(bindir + "/mysql_install_db.exe", args);

            if (ret == 0) {
                QSettings set(dbdir + "/my.ini", QSettings::IniFormat);
                set.beginGroup("mysqld");
                set.setValue("character-set-server", "utf8");
                set.endGroup();
            }
        }
    }

    return ret;
}
