cmake_minimum_required(VERSION 3.16)

project(mysqldbcfg)

if(WIN32)
    SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} /SUBSYSTEM:WINDOWS,5.01")
endif(WIN32)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTORCC ON)

include_directories(${CMAKE_CURRENT_BINARY_DIR}
                    ${CMAKE_CURRENT_SOURCE_DIR}
                    )


set(SOURCES
    main.cpp
    )


find_package(Qt5Core)
find_package(Qt5Xml)

set(LIBS ${LIBS} Qt5::Core Qt5::Xml)

add_executable(${PROJECT_NAME} ${SOURCES})

target_link_libraries(${PROJECT_NAME} ${LIBS})

